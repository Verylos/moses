module Graph.Graph
( Graph(Graph)
, Project(Project)
, getNodes
, getFacts
, getAssumptions
, getPremiseMap
, getGoalMap
, getGraphDB
, getGraph
, setNodeValue
, getNodeValue
, entityNodeList
, getGoalsByNode
, getGoalsByPremise
, getLeftSidesByGoal
, getLeftSidesByPremise
, evaluateGoals
, evalGoal
, TargetId
, SourceId
) where

import Import
import Data.Aeson ()
import qualified Data.Map as M
import Graph.Premise
import Data.List as L

{-
Graphs should not get too large here, so we maintain the luxus of fast goal/premise finding
-}

type SourceId = DBNodeId
type TargetId = DBNodeId

type NodeMap    = M.Map DBNodeId DBNode
type PremiseMap = M.Map TargetId [[Premise]]
type GoalMap    = M.Map SourceId [TargetId]
type Relation   = (DBRuleId, TargetId, Premise)
type Rule       = (TargetId, [Premise])

data Graph = Graph
  { getNodes       :: NodeMap
  , getFacts       :: [SourceId]
  , getAssumptions :: [DBNodeId]
  , getPremiseMap  :: PremiseMap -- find all left rule sides for a goal
  , getGoalMap     :: GoalMap    -- find all goals for a left side
  } deriving (Show)

data Project = 
  Project [Entity DBNode] [Entity DBRule] [Entity DBCondition] [Entity DBConclusion] deriving (Show)

instance ToJSON Project where
  toJSON (Project nodes rules conds concs) =
    object [ "nodes" .= nodes
           , "rules" .= rules
           , "conds" .= conds
           , "concs" .= concs ]

instance FromJSON Project where
  parseJSON (Object v) = 
      Project <$> v .: "nodes"
              <*> v .: "rules"
              <*> v .: "conds"
              <*> v .: "concs"
  parseJSON _ = error "Could not parse project input"

getGraphDB :: [(Text, Value)] -> Handler Value
getGraphDB msgs = do
  nodes <- runDB $ selectList ([] :: [Filter DBNode]) []
  rules <- runDB $ selectList ([] :: [Filter DBRule]) []
  conds <- runDB $ selectList ([] :: [Filter DBCondition]) []
  concs <- runDB $ selectList ([] :: [Filter DBConclusion]) []
  return $ object $ [ "nodes" .= nodes
                    , "rules" .= rules
                    , "conds" .= conds
                    , "concs" .= concs ] ++ msgs

-- sql please.. y u no join? :(
getGraph :: Handler Graph
getGraph = do
  nodeL <- runDB $ selectList ([] :: [Filter DBNode])       []
  condL <- runDB $ selectList ([] :: [Filter DBCondition])  []
  concL <- runDB $ selectList ([] :: [Filter DBConclusion]) []
  let nodeMap      = M.fromList $ map (\(Entity id' node) -> (id', node)) nodeL  -- :: NodeMap
      relations    = getValidRelations (map extract condL) (map extract concL)   -- :: [Relation]
      rules        = combineRelations relations                                  -- :: [Rule]
      premiseMap'  = foldr createPremiseMap M.empty rules                        -- :: PremiseMap
      goalMap'     = foldr createGoalMap M.empty relations
      premises     = M.keys goalMap'                                             -- :: [SourceId]
      goals        = L.nub $ concat $ M.elems goalMap'                           -- :: [TargetId]
      facts'       = extractFacts goals premises                                 -- :: [SourceId]
      assumptions' = L.nub (goals ++ premises) L.\\ facts'
  return $ Graph nodeMap facts' assumptions' premiseMap' goalMap'

extract :: Entity a -> a
extract (Entity _ x) = x

{-
Evaluates the list of goals and returns an upgraded graph.
-}

evaluateGoals :: Graph -> [TargetId] -> Maybe Graph
evaluateGoals graph = foldr evalGoal (Just graph)

{-
Evaluates a single goal and returns an upgraded graph.
-}

evalGoal :: TargetId -> Maybe Graph -> Maybe Graph
evalGoal goal mgraph = mgraph >>= \ graph' ->
    case (evalLeftSides (getNodes graph') (getLeftSidesByGoal graph' goal)) of
      Nothing      -> Nothing
      Just Unknown -> Just graph'
      Just newVal  -> Just $ setNodeValue graph' goal newVal

{-
Combine relations to rule tuples, for which a target is associated with
a list of premises.
Build a map with (rule, target) as key and a list of premises as value.
Afterwards these are combined for each rule
-}

combineRelations :: [Relation] -> [Rule]
combineRelations relations = M.foldWithKey combineMap [] relationMap
  where combineMap (_, target) premises = ((target,premises):)
        relationMap = foldr combinePremises M.empty relations                    -- :: Map (RuleId, TargetId) [Premise]
        combinePremises (rule,target,premise) =
                                      M.insertWith (++) (rule, target) [premise]

{-
Create a map to get all premises for a goal
-}

createPremiseMap :: Rule -> PremiseMap -> PremiseMap
createPremiseMap (target, premises) = M.insertWith (++) target [premises]

getGoalsByNode :: Graph -> SourceId -> [TargetId]
getGoalsByNode graph premise = maybeToList $
                                      M.lookup premise (getGoalMap graph)

{-
Find all goals for which the premise is a condition
-}

getGoalsByPremise :: Graph -> Premise -> [TargetId]
getGoalsByPremise graph (id',_) = maybeToList $
                                      M.lookup id' (getGoalMap graph)

{-
Find premises to a premise interpreted as goal
-}

getLeftSidesByPremise :: Graph -> Premise -> [[Premise]]
getLeftSidesByPremise graph (id',_) = maybeToList $
                                      M.lookup id' (getPremiseMap graph)

{-
Find premises to a goal
-}

getLeftSidesByGoal :: Graph -> TargetId -> [[Premise]]
getLeftSidesByGoal graph node = maybeToList $
                                      M.lookup node (getPremiseMap graph)

{-
Get the value of a node
-}

getNodeValue :: Graph -> DBNodeId -> Maybe Truth
getNodeValue graph nodeId = case M.lookup nodeId (getNodes graph) of
  Just (DBNode _ value _ _) -> Just value
  Nothing    -> Nothing

{-
Set a specific value of a node
-}

setNodeValue :: Graph -> DBNodeId -> Truth -> Graph
setNodeValue (Graph nodes' facts' assump' premM' goal') goal value
  = (Graph (M.update (\(DBNode name _ x y)
  -> Just (DBNode name value x y)) goal nodes') facts' assump' premM' goal')

{-
Get all entity nodes
-}

entityNodeList :: Graph -> [Entity DBNode]
entityNodeList graph = map (\(a,b)-> Entity a b) $ M.assocs (getNodes graph)

{-
Get all sources which are not targets
-}

extractFacts :: [TargetId] -> [SourceId] -> [SourceId]
extractFacts goals premises = L.nub $
                     [premise | premise <- premises, not $ L.elem premise goals]

{-
Creates a map for each source node to list of targets
-}

createGoalMap :: Relation -> GoalMap -> GoalMap
createGoalMap (_, target, (source, _))
                                   = M.insertWith ((++) . L.nub) source [target]

{-
Builds relations for which for each source node exists a condition and rule
and for each conclusion for that rule exists a target.
-}

getValidRelations :: [DBCondition] -> [DBConclusion] -> [Relation]
getValidRelations conds concs =
  let sources = map (\(DBCondition source rule unless)
                                        -> (rule, (source, unless))) conds
      targets = map (\(DBConclusion rule target) -> (rule, target))  concs
  in [ (rule, target, (source, unless)) | (rule, (source, unless)) <- sources
                                  , (rule', target) <- targets, rule == rule' ]

{-

-}

maybeToList :: Maybe [a] -> [a]
maybeToList (Just xs) = xs
maybeToList Nothing   = []
