module Graph.Premise
  ( Premise
  , eval
  , evalLeftSides
  ) where

import Import

import Data.Maybe
import qualified Data.Map as M
import qualified Data.List as L

{-
Premise: tuple of id for a DBNode and a flag for unless classification
-}
type Premise = ( DBNodeId, Bool )

{-
eval: fetches the DBNode for the id from the supplied nodemap and trs to evaluate it according to the unless context
returns Just value on success and Nothing if the node is not existing
-}
eval :: M.Map DBNodeId DBNode -> Premise -> Maybe Truth
eval nodes (nodeId, isUnless) =
   fmap (\(DBNode _ value _ _) -> if isUnless then negateTruth value else value)
        (M.lookup nodeId nodes)

{-
evalList: if a node wasnt found -> Nothing, combines the values accordingly and returns Just value otherwise
-}

evalRule :: M.Map DBNodeId DBNode -> [Premise] -> Maybe Truth
evalRule nodes premises = withJust andTruth $ map (eval nodes) premises

{-
evalLeftSides: if a node wasnt found -> Nothing, Just value with value as the information if a rule can be applied to a goal
(use this for forward chaining)
-}

evalLeftSides :: M.Map DBNodeId DBNode -> [[Premise]] -> Maybe Truth
evalLeftSides nodes leftSides = case leftSides of
  [] -> Just Unknown
  _  -> withJust orTruth $ map (evalRule nodes) leftSides

withJust :: Eq a => ([a] -> b) -> [Maybe a] -> Maybe b
withJust f ms = if L.elem Nothing ms then Nothing
                else Just $ f $ map fromJust ms
