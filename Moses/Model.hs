{-# LANGUAGE FlexibleInstances #-}
module Model where

import Yesod
import Data.Text (Text)
import Prelude (Double, Show)
import Database.Persist.Quasi
import Data.Typeable (Typeable)
import Control.Applicative ((<$>),(<*>))
import Database.Persist.Sql (fromSqlKey)
import Prelude
import DB.Truth

-- You can define all of your database entities in the entities file.
-- You can find more information on persistent and how to declare entities
-- at:
-- http://www.yesodweb.com/book/persistent/
share [mkPersist sqlSettings, mkDeleteCascade sqlSettings, mkMigrate "migrateAll"]
    $(persistFileWith lowerCaseSettings "config/models")    
    
{-
Database toJSON and fromJSON instances belong here to avoid orphan instance warnings (it seems to be model.hs, where database datatypes live)
Also, Entities only get a ToJSON instance. This is because Entities never get accepted as is from the frontend.
-}

instance ToJSON (Entity DBNode) where
  toJSON (Entity id' (DBNode name value x y)) = 
      object [ "id"    .= (fromSqlKey id') 
             , "name"  .= name
             , "value" .= value
             , "x"     .= x
             , "y"     .= y ]

instance FromJSON (Entity DBNode) where
  parseJSON (Object v) = 
      Entity <$> v .: "id" 
             <*> (DBNode <$> v .: "name" 
                         <*> v .: "value" 
                         <*> v .: "x" 
                         <*> v .: "y" )
  parseJSON _ = error "Could not parse entity DBNode"



instance ToJSON (Entity DBRule) where
  toJSON (Entity id' (DBRule x y)) = 
      object [ "id" .= (fromSqlKey id')
             , "x"  .= x
             , "y"  .= y ]

instance FromJSON (Entity DBRule) where
  parseJSON (Object v) = 
      Entity <$> v .: "id" 
             <*> (DBRule <$> v .: "x" 
                         <*> v .: "y" )
  parseJSON _ = error "Could not parse entity DBRule"



instance ToJSON (Entity DBCondition) where
  toJSON (Entity id' (DBCondition node rule neg)) = 
      object [ "id"     .= (fromSqlKey id')
             , "source" .= node
             , "ruleId" .= rule
             , "negate" .= neg ]

instance FromJSON (Entity DBCondition) where
  parseJSON (Object v) = 
      Entity <$> v .: "id" 
             <*> (DBCondition <$> v .: "source" 
                              <*> v .: "ruleId"
                              <*> v .: "negate" )
  parseJSON _ = error "Could not parse entity DBCondition"
  
    
   
instance ToJSON (Entity DBConclusion) where
  toJSON (Entity id' (DBConclusion rule node)) = 
      object [ "id"     .= (fromSqlKey id')
             , "ruleId" .= rule
             , "target" .= node ]
                                                          
instance FromJSON (Entity DBConclusion) where
  parseJSON (Object v) = 
      Entity <$> v .: "id" 
             <*> (DBConclusion <$> v .: "ruleId" 
                               <*> v .: "target" )
  parseJSON _ = error "Could not parse entity DBConclusion"

