{-# LANGUAGE RankNTypes #-}
module Handler.Import where

import Import
import Graph.Graph as G
import Control.Monad (forM)
import DB.DB as D
import qualified Data.List as L

postImportR :: Handler Value
postImportR = do
  project <- requireJsonBody :: Handler (Maybe Project)
  response <- case project of
    Just (Project nodes rules conds concs) -> do
      let nodes' = map (\(Entity id' _) -> id') nodes
          rules' = map (\(Entity id' _) -> id') rules
      case (wrongConds nodes' rules' conds, wrongConcs nodes' rules' concs) of
        ([],[])         -> do
          D.deleteDB
          nidMapping <- transferSimple nodes
          ridMapping <- transferSimple rules
          boolCond   <- constructConds nidMapping ridMapping conds
          boolConc   <- constructConcs nidMapping ridMapping concs
          case and $ boolCond ++ boolConc of
            True -> do
              r <- G.getGraphDB ["messages" .= Null]
              return r
            False -> do
              r <- G.getGraphDB ["messages" .= (object ["importError" .= Null])]
              return r
        (wConds,wConcs) -> do
          r <- G.getGraphDB $
            ["messages" .= (object ["projectError" .= object
              ["conditions" .= wConds, "conclusions" .= wConcs]])]
          return r
    Nothing -> do
      r <- G.getGraphDB ["messages" .= (object ["parseError" .= Null])]
      return r
  return . toJSON $ response

-- transfer simple things (nodes and rules)
transferSimple ::
  forall site t.(PersistStore (YesodPersistBackend site),
    YesodPersist site, PersistEntityBackend t ~ YesodPersistBackend site) =>
      [Entity t] -> HandlerT site IO [(Key t, Key t)]
transferSimple list = runDB $ (forM list $ \(Entity oldId subject) -> do
  newId <- insert subject
  return (oldId,newId))

constructConds :: [(DBNodeId, DBNodeId)]
  -> [(DBRuleId, DBRuleId)]
  -> [Entity DBCondition]
  -> Handler [Bool]
constructConds nidMapping ridMapping conds =
  runDB $ (forM conds $ \(Entity _ (DBCondition source rule isUnless)) -> do
    let source' = findNode source nidMapping
    let rule'   = findRule rule ridMapping
    case (source',rule') of
      (Just s, Just r) -> do
        _ <- insert $ DBCondition s r isUnless
        return True
      _                -> do
        return False
    )

constructConcs :: [(DBNodeId, DBNodeId)]
  -> [(DBRuleId, DBRuleId)]
  -> [Entity DBConclusion]
  -> Handler [Bool]
constructConcs nidMapping ridMapping concs =
  runDB $ (forM concs $ \(Entity _ (DBConclusion rule target)) -> do
    let rule' = findRule rule ridMapping
    let target' = findNode target nidMapping
    case (rule', target') of
      (Just r, Just t) -> do
        _ <- insert $ DBConclusion r t
        return True
      _                -> do
        return False
    )

wrongConds :: [DBNodeId]
  -> [DBRuleId]
  -> [Entity DBCondition]
  -> [DBConditionId]
wrongConds _ _ [] = []
wrongConds nodes rules ((Entity cid (DBCondition source rule _)):conds) =
  case and [L.elem source nodes, L.elem rule rules] of
    True  -> wrongConds nodes rules conds
    False -> cid:(wrongConds nodes rules conds)

wrongConcs :: [DBNodeId]
  -> [DBRuleId] -> [Entity DBConclusion] -> [DBConclusionId]
wrongConcs _ _ [] = []
wrongConcs nodes rules ((Entity cid (DBConclusion rule target)):concs) =
  case and [L.elem target nodes, L.elem rule rules] of
    True  -> wrongConcs nodes rules concs
    False -> cid:(wrongConcs nodes rules concs)

findNode :: DBNodeId -> [(DBNodeId,DBNodeId)] -> Maybe DBNodeId
findNode id' nodes = case nodes of
  [] -> Nothing
  ((oid,nid):ns) -> if id' == oid
    then Just nid
    else findNode id' ns

findRule :: DBRuleId -> [(DBRuleId,DBRuleId)] -> Maybe DBRuleId
findRule rid rules = case rules of
  [] -> Nothing
  ((orid,nrid):rs) -> if rid == orid
    then Just nrid
    else findRule rid rs
