module Handler.Rule where

import Import
import Control.Monad (forM)
import Graph.Graph as G


postRuleR :: Handler Value
postRuleR = do
  mRule <- requireJsonBody :: Handler (Maybe [Entity DBRule])
  case mRule of
    Nothing -> invalidArgs ["could not parse list of rules"]
    Just rules -> do
      _ <- forM rules $ \(Entity id' (DBRule x y)) ->
        runDB $ update id' [ DBRuleX  =. x 
                           , DBRuleY  =. y ]
      graph <- G.getGraphDB []
      return . toJSON $ graph
        

putRuleR :: Handler Value
putRuleR = do
  mRule <- requireJsonBody :: Handler (Maybe [Entity DBRule])
  case mRule of
    Nothing -> invalidArgs ["could not parse list of rules"]
    Just rules -> do
      _ <- forM rules $ \(Entity _ rule) ->
        runDB . insert $ rule
      graph <- G.getGraphDB []
      return . toJSON $ graph
    
    
deleteRuleR :: Handler Value
deleteRuleR = do
  mRule <- requireJsonBody :: Handler (Maybe [Entity DBRule])
  case mRule of
    Nothing -> invalidArgs ["could not parse list of rules"]
    Just rules -> do
      _ <- forM rules $ \(Entity id' _) ->
        runDB $ deleteCascade id'
      graph <- G.getGraphDB []
      return . toJSON $ graph

