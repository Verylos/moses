module Handler.Node where

import Import
import Control.Monad (forM)
import Graph.Graph as G
import DB.DB as D

postNodeR :: Handler Value
postNodeR = do
  mNode  <- requireJsonBody :: Handler (Maybe [Entity DBNode])
  case mNode of
    Nothing -> invalidArgs ["could not parse list of nodes"]
    Just nodes -> do 
      D.editNodes nodes
      graph <- G.getGraphDB []
      return . toJSON $ graph

putNodeR :: Handler Value
putNodeR = do
  mNode <- requireJsonBody :: Handler (Maybe [Entity DBNode])
  case mNode of
    Nothing -> invalidArgs ["could not parse list of nodes"]
    Just nodes -> do
      D.addNodes nodes
      graph <- G.getGraphDB []
      return . toJSON $ graph


deleteNodeR :: Handler Value
deleteNodeR = do
  mNode <- requireJsonBody :: Handler (Maybe [Entity DBNode])
  case mNode of
    Nothing -> invalidArgs ["could not parse list of nodes"]
    Just nodes -> do
      _ <- forM nodes $ \(Entity id' _) ->
        runDB $ deleteCascade id'
      graph <- G.getGraphDB []
      return . toJSON $ graph
    
