module Handler.DB where

import Import
import Graph.Graph as G
import Database.Persist.Sql 

getDBR :: Handler Html
getDBR = do
    nodes <- runDB $ selectList ([] :: [Filter DBNode]) []
    rules <- runDB $ selectList ([] :: [Filter DBRule]) []
    conds <- runDB $ selectList ([] :: [Filter DBCondition]) []
    concs <- runDB $ selectList ([] :: [Filter DBConclusion]) []
    _ <- G.getGraphDB []
    defaultLayout [whamlet|
        <h2> debug database
        <p> nodes:
        <ul>
            $forall (Entity nid (DBNode name value x y)) <- nodes
                <li>
                    <p> nodeID: #{fromSqlKey nid} , name: #{name} , value: #{show value} , x: #{x} , y: #{y} 
        <br>
        <p> rules:
        <ul>
            $forall (Entity rid (DBRule x y)) <- rules
                <li>
                    <p> ruleID: #{fromSqlKey rid} , x: #{x} , y: #{y}
        <br>
        <p> conditions:
        <ul>
            $forall (Entity cid (DBCondition sid rid neg)) <- conds
                <li>
                    <p> conditionID: #{fromSqlKey cid} , sourceID: #{fromSqlKey sid} , ruleID: #{fromSqlKey rid} , negate: #{neg}
        <br>
        <p> conclusions:
        <ul>
            $forall (Entity cid (DBConclusion rid tid )) <- concs
                <li>
                    <p> conclusionID: #{fromSqlKey cid} , ruleID: #{fromSqlKey rid} , targetID: #{fromSqlKey tid}
    |]
    
postDBR :: Handler Value
postDBR = error "fuk u"
