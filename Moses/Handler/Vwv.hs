module Handler.Vwv where

import Import

import Graph.Graph as G
import Graph.Premise
import DB.DB as D
import Data.List as L

-- import qualified Data.Map as M

postVwvR :: Handler Value
postVwvR = do
  graph <- G.getGraph
  let result = vwv
         (L.nub $ orderFacts graph (G.getFacts graph) ++ G.getAssumptions graph)
         (OK graph [])
  response <- case result of
    OK graph' _ -> do 
      D.editNodes (G.entityNodeList graph')
      r <- G.getGraphDB ["messages" .= Null] 
      return r 
    Fail loops -> do 
      r <- G.getGraphDB ["messages" .= (object [ "loops" .= loops ])]
      return r
    Missing nodes -> do
      r <- G.getGraphDB ["messages" .= (object [ "missing" .= nodes ])]
      return r 
  return response

type Path  = [(TargetId, [Premise])]
type Chain = [Path]
data Loop  = Regular Path
           | NonMonotonousOdd  Path
           | NonMonotonousEven Path
  deriving (Show, Eq)

instance ToJSON Loop where
  toJSON (Regular path)           = object [ "Regular"           .= path ]
  toJSON (NonMonotonousOdd path)  = object [ "NonMonotonousOdd"  .= path ]
  toJSON (NonMonotonousEven path) = object [ "NonMonotonousEven" .= path ]

data Result = OK Graph [Loop]
            | Fail     [Loop]
            | Missing  [DBNodeId]
  deriving (Show)

orderFacts :: Graph -> [SourceId] -> [SourceId]
orderFacts graph = L.sortBy
            (\ x y -> compare (G.getNodeValue graph x) (G.getNodeValue graph y))

firstIs :: Eq a => a -> (a, b) -> Bool
firstIs seed (x,_) = x == seed

{-
vwv
- starts forward chaining for each node in the todo list
-}

vwv :: [DBNodeId] -> Result -> Result
vwv todo result = case (result, todo) of
  (OK graph _, (start:rest)) -> vwv rest $ forwardChain result [start] []
                                         [[(start, getAllPremises graph start)]]
  _ -> result

{-
forwardChain
- evaluates current goals and initiates next step
-}

forwardChain :: Result -> [DBNodeId] -> [DBNodeId] -> Chain -> Result
forwardChain result currentGoals seenGoals chain = case result of
  OK graph loops -> case currentGoals of
    [] -> result
    _  -> case ( G.evaluateGoals graph currentGoals ) of
      Just graph' -> stepForward graph' currentGoals seenGoals chain loops
      Nothing -> Missing []
  other -> other

{-
stepForward
- try to get new goals going out from the given nodes in 'currents'
- if a new goal was already visited -> classify the loop and return early iff
  an odd nonmonotounus loop was found
- if everything went well: forward chain with new goals
-}

stepForward :: Graph -> [TargetId] -> [TargetId] -> Chain -> [Loop] -> Result
stepForward graph currents visited chain loops =
  let nextGoals = L.nub $ L.concatMap (G.getGoalsByNode graph) currents
  in if or $ map (flip L.elem visited) nextGoals
     then case findAllCycles chain $ getAssocs graph currents of
       []     -> Fail []
       loops' -> if checkLoops loops' then OK graph loops' else Fail loops'
     else forwardChain (OK graph loops) nextGoals (visited ++ currents)
                       (expandChain graph currents chain)

{-
getAssocs
- get associations from the set of current goals to all nodes reachable from 
  that set (used only for finding loops)
- resulting in a list of pairs [(reachable node, from current node)]
-}

getAssocs :: Graph -> [TargetId] -> [(TargetId, SourceId)]
getAssocs _ [] = []
getAssocs graph (g:gs) = case G.getGoalsByNode graph g of
    []    -> getAssocs graph gs
    goals -> [(ng, g) | ng <- goals] ++ getAssocs graph gs

{-
findPaths
- search in the given chain for a path, where the given node is in the first
  tuple

basically: look in all the paths we walked if this node was the last we added
 and return this path
-}

findPaths :: DBNodeId -> Chain -> [Path]
findPaths newGoal = filter (\ path -> fst (head path) == newGoal)

{-
findCycles
- find all paths to the current node (old)
- filter all paths where the new goal was already visited (there must be a loop
  in this path)
- classify the loops for the pathpieces which are in fact a loop (takeWhile ...)
-}

findCycles :: TargetId -> TargetId -> Chain -> [Loop]
findCycles new old = map classify . filter isRelevant . findPaths old
  where isRelevant = elem new . map fst
        classify   = classifyLoop . takeWhileInclusive (\(x,_) -> new /= x)

{-
findAllCycles
- FIND ALL THE CYCLES
- so basically look for each (new goal, old goal) combination, where the loops
  the loops sit
-}

findAllCycles :: Chain -> [(TargetId, TargetId)] -> [Loop]
findAllCycles chain = L.concatMap (\(x,y) -> findCycles x y chain)

{-
classifyLoop
- counts the unless premises in a path known to contain a loop
- first the path is aligned, so that the goal and premises this goal is reached
  by stand in one pair
- filter only the premises, which contain the goal
- count unless

-}

classifyLoop :: Path -> Loop
classifyLoop path = if unlessCount == 0 then Regular path
                    else if even unlessCount then NonMonotonousEven path
                                             else NonMonotonousOdd path
  where unlessCount = length $ filter snd $ L.concatMap
                         (\(goal, premises) -> filter (firstIs goal) premises) $
                         loopZip (fst $ head path) path
{-
loopZip
- basically: zip [1,2,3] $ shiftRotateRight [[not2],[not3],[1]]
  => [(1,[1]),(2,[not2]),(3,[not3])] <- so only filter fst out of snd and count 
     false premises
- goals are directly aligned to the premises they are reached from to count
  unless rules inside the loop
-}

loopZip :: TargetId -> Path -> Path
loopZip _ [] = []
loopZip start [(_, premises)] = [(start,premises)]
loopZip start ((_, premises) : (goal, premises') : rest) =
                   (goal, premises) : (loopZip start $ (goal, premises') : rest)

checkLoops :: [Loop] -> Bool
checkLoops = and . map checkLoop
  where checkLoop (NonMonotonousOdd _) = False
        checkLoop _ = True

takeWhileInclusive :: (a -> Bool) -> [a] -> [a]
takeWhileInclusive _ [] = []
takeWhileInclusive p (x:xs) = x : if p x then takeWhileInclusive p xs else []

getAllPremises :: Graph -> TargetId -> [Premise]
getAllPremises graph = L.concat . G.getLeftSidesByGoal graph

{-
expandChain
- for all current goals: prepend (new goal,[premises]) to a path in the chain 
  if the head of the path is a current goal

-}

expandChain :: Graph -> [TargetId] -> Chain -> Chain
expandChain graph currentGoals chain = foldr expand chain currentGoals
  where expand goal chain' = expandPaths chain' goal $ getNewGoals graph goal

expandPaths :: Chain -> TargetId -> Path -> Chain
expandPaths oldChain oldGoal newPath = case oldChain of
  (premise@(goal, _):restPath):restChain ->
    ( if goal == oldGoal then map (:premise:restPath) newPath
                        else [premise:restPath] )
           ++ expandPaths restChain oldGoal newPath
  _ -> []

getNewGoals :: Graph -> TargetId -> Path
getNewGoals graph oldGoal = map (\goal -> (goal, getAllPremises graph goal))
                                (L.nub $ G.getGoalsByNode graph oldGoal)

{-
extractGoalsFromChain :: Chain -> [DBNodeId]
extractGoalsFromChain chain = L.nub $ map (\path -> fst $ (head path)) chain
-}
