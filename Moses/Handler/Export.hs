module Handler.Export where

import Import
import Graph.Graph as G

postExportR :: Handler Value
postExportR = do
  nodes <- runDB $ selectList ([] :: [Filter DBNode]) []
  rules <- runDB $ selectList ([] :: [Filter DBRule]) []
  conds <- runDB $ selectList ([] :: [Filter DBCondition]) []
  concs <- runDB $ selectList ([] :: [Filter DBConclusion]) []
  return $ toJSON $ Project nodes rules conds concs
