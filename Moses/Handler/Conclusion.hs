module Handler.Conclusion where

import Import
import Prelude (and)
import Control.Monad (forM)
import Graph.Graph as G

postConclusionR :: Handler Value
postConclusionR = do
  mConc <- requireJsonBody :: Handler (Maybe [Entity DBConclusion])
  case mConc of
    Nothing -> invalidArgs ["could not parse list of conclusions"]
    Just concs -> do
      invalid <- invalidLinks concs
      if invalid 
        then invalidArgs ["expected rules and/or targets do not exist"]
        else do
          _ <- forM concs $ \(Entity id' (DBConclusion rule target)) -> do
            runDB $ update id' [ DBConclusionRuleId =. rule
                               , DBConclusionTarget =. target ]
          graph <- G.getGraphDB []
          return . toJSON $ graph
    

putConclusionR :: Handler Value
putConclusionR = do
  mConc <- requireJsonBody :: Handler (Maybe [Entity DBConclusion])
  case mConc of
    Nothing -> invalidArgs ["could not parse list of conclusions"]
    Just concs -> do
      invalid <- invalidLinks concs
      if invalid 
        then invalidArgs ["expected rules and/or targets do not exist"]
        else do
          _ <- forM concs $ \(Entity _ conc) ->
            runDB $ insert conc
          graph <- G.getGraphDB []
          return . toJSON $ graph        


deleteConclusionR :: Handler Value
deleteConclusionR = do
  mConc <- requireJsonBody :: Handler (Maybe [Entity DBConclusion])
  case mConc of
    Nothing -> invalidArgs ["could not parse list of conclusions"]
    Just concs -> do
      _ <- forM concs $ \(Entity id' _) -> do
          runDB $ delete id'
      graph <- G.getGraphDB []
      return . toJSON $ graph


invalidLinks :: [Entity DBConclusion] -> Handler Bool
invalidLinks concs = do
  invalidities <- forM concs $ \(Entity _ (DBConclusion rule target)) -> do
    mRule <- runDB $ get rule
    mNode <- runDB $ get target
    case (mRule, mNode) of
      (Just _, Just _) -> return False
      _                -> return True
  return $ or invalidities
