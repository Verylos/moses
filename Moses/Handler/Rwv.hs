module Handler.Rwv where

import Import
import Graph.Graph as G
import Graph.Premise
import DB.DB as D
import Data.List as L

postRwvR :: Handler Value
postRwvR = do
  (Entity goalId _) <- requireJsonBody :: Handler (Entity DBNode)
  searched <- runDB $ get goalId
  case searched of
    Nothing -> error "Node for backward chaining does not exist"
    _ -> do
      graph  <- G.getGraph
      let contextTree = getContextTree goalId graph
      let allLoops    = L.nub $ map classifyLoop 
                        $ L.concatMap findLoopPaths contextTree
      case filter isBadLoop allLoops of
        []     -> case rwv (Just (graph, [])) contextTree goalId allLoops of
          Just (graph', [])    -> do
            D.editNodes (G.entityNodeList graph')
            r <- G.getGraphDB ["messages" .= Null]
            return r
          Just (graph', nodes) -> do
            D.editNodes (G.entityNodeList graph')
            r <- G.getGraphDB ["messages" .= (object [ "ask" .= nodes ])]
            return r
          Nothing              -> do
            r <- G.getGraphDB ["messages" .= (object [ "error" .= Null ])]
            return r
        (l:ls) -> do
          r <- G.getGraphDB ["messages" .= (object [ "loops" .= (l:ls) ])]
          return r

data ContextTree =  ContextNode Premise
                  | ContextLoop Premise
                  | Branch Premise [ContextTree] deriving (Show)

type Path  = [Premise]
data Loop  = Regular Path
           | NonMonotonousOdd  Path
           | NonMonotonousEven Path
  deriving (Show)

data Evaluable = EvalAnd [DBNodeId] | EvalOr [DBNodeId] deriving (Show, Eq)

{-
rwv: main function which executes the backward chaining.
Foreach context list representing a rule which could prove the goal rwv
will try to evaluate the rule. If the goal is proven afterwards the
backward chaining is over. If the goal is not proven rwv will be applied to
the remaining contexts recursively.

If the empty list is passed rwv tries to evaluate the goal once more.
If the goal is unprovable there is no need to ask for node values and
backward chaining is done.
Otherwise the goal was set by an assumption and we need to ask for more
knowledge

returns a tuple (updated graph, nodelist with all nodes to ask for) on
success and Nothing on failure
-}

rwv :: Maybe (Graph, [DBNodeId])
  -> [[ContextTree]] 
  -> TargetId 
  -> [Loop] 
  -> Maybe (Graph, [DBNodeId]) 
rwv Nothing _ _ _ = Nothing
rwv (Just (graph, toAsk)) contexts goalId loops =
  case contexts of
    []   -> case G.evalGoal goalId (Just graph) of
      Just graph' -> if G.getNodeValue graph' goalId == Just Neg
        then if and 
              $ map (hasValue graph') 
              $ map fst $ L.concat $ G.getLeftSidesByGoal graph' goalId
          then (Just (graph', []))
          else (Just (graph', L.nub $ toAsk))
        else (Just (graph', L.nub toAsk))
      Nothing     -> Nothing
    c:cs -> case evalContexts (Just graph) c loops of
      Nothing     -> Nothing
      Just graph' -> case isSuccess 
                      $ map (eval $ G.getNodes graph') $ getContextPremises c of
        Just True -> Just (G.setNodeValue graph' goalId Pos, [])
        _         -> let next = getContext c (map getPath loops)
                     in rwv (Just (graph', filter (not . (hasValue graph')) 
                        $ L.nub $ toAsk++next)) cs goalId loops


instance ToJSON Loop where
  toJSON (Regular path)           = object [ "Regular"           .= path ]
  toJSON (NonMonotonousOdd path)  = object [ "NonMonotonousOdd"  .= path ]
  toJSON (NonMonotonousEven path) = object [ "NonMonotonousEven" .= path ]

instance Eq Loop where
  (Regular x) == (Regular y) = setEqual x y
  (NonMonotonousOdd x) == (NonMonotonousOdd y) = setEqual x y
  (NonMonotonousEven x) == (NonMonotonousEven y) = setEqual x y
  _ == _ = False

setEqual :: Eq a => [a] -> [a] -> Bool
setEqual x y = 
  and $ L.concat $ [map (\e -> L.elem e y) x, map (\e -> L.elem e x) y] 

{-
Helper function
Used to check if a rule fires.
-}

isSuccess :: [Maybe Truth] -> Maybe Bool
isSuccess [] = Just True
isSuccess (t:ts) = case t of
  Just Pos -> isSuccess ts
  Nothing  -> Nothing
  _        -> Just False

{-
Get the first level premises of a context
-}

getContextPremises :: [ContextTree] -> [Premise]
getContextPremises [] = []
getContextPremises (c:cs) = (getContextPremises cs) ++
  case c of
    ContextNode premise -> [premise]
    ContextLoop premise -> [premise]
    Branch premise _    -> [premise]

{-
Get all nodes of the context also containing loops.
-}

getContext :: [ContextTree] -> [Path] -> [DBNodeId]
getContext contexts loops =  L.nub $ L.concat $ do
  context <- contexts
  case context of
    ContextNode p   -> return [fst p]
    ContextLoop p   -> do 
      loop <- map (\l -> map fst l) $ (filter (\l -> L.elem p l) loops)
      return loop
    Branch _ trees  -> do 
      next <- getContext trees loops
      return [next]

{-
If a node is not unknown, it has a value
-}

hasValue :: Graph -> DBNodeId -> Bool
hasValue graph n  = case G.getNodeValue graph n of
  Just Pos -> True
  Just Neg -> True
  _        -> False

{-
Translate a loop to a premise list
-}

getPath :: Loop -> Path
getPath (Regular path)            = path
getPath (NonMonotonousOdd path)   = path
getPath (NonMonotonousEven path)  = path

{-
For each rule which has the goal as target, construct the context.
-}

getContextTree :: TargetId -> Graph -> [[ContextTree]]
getContextTree goal graph = let premises = G.getLeftSidesByGoal graph goal
  in case premises of
    [] -> []
    _  -> map (\p -> constructTree graph p [goal]) premises

{-
Construct the context to a rule (premise list) as a tree.
If loops are found, they are marked as ContextLoop nodes. This indicates
that the looping node is found twice in this sub tree.
-}

constructTree :: Graph -> [Premise] -> [DBNodeId] -> [ContextTree]
constructTree graph premises seen = do
  premise <- premises
  let premiseId = fst premise
  if L.elem premiseId seen
    then return $ ContextLoop premise
    else let next = L.concat $ G.getLeftSidesByGoal graph premiseId
         in case next of
           [] -> return $ ContextNode premise
           _  -> return $ Branch premise $ 
              constructTree graph next (premiseId:seen)

{-
Evaluates a context and gives back an updated graph on success.
-}

evalContexts :: Maybe Graph -> [ContextTree] -> [Loop] -> Maybe Graph
evalContexts Nothing _ _ = Nothing
evalContexts mGraph context loops = case context of
  (c:cs) -> evalContexts (evalContext mGraph c loops) cs loops
  []     -> mGraph

{-
Evaluates parts of a context by evaluating branching nodes.
If a loop is encountered not only the branch node, but all nodes on the loop
get evaluated.
-}

evalContext :: Maybe Graph -> ContextTree -> [Loop] -> Maybe Graph
evalContext Nothing _ _ = Nothing
evalContext mGraph context loops = case context of
    Branch (id', _) tree -> 
      let nodes = (map fst $ L.concatMap getPath $ filter (elemLoop id') loops)
      in foldr G.evalGoal (evalContexts mGraph tree loops) (id':nodes)
    _ -> mGraph    

{-
Find looping paths in the context but without classification.
-}

findLoopPaths :: [ContextTree] -> [Path]
findLoopPaths trees = case trees of
  [] -> []
  _  -> L.concatMap (\t -> stepThrough [] t) trees

{-
Step through the context until you find a looping node.
If so, return the path to that node, otherwise no path is returned
-}

stepThrough :: Path -> ContextTree -> [Path]
stepThrough path tree = case tree of
  ContextNode _   -> []
  ContextLoop p   -> if L.elem p path
    then [takeWhileInclusive (\p' -> p' /= p) path]
    else [p:path]
  Branch p trees  -> L.concatMap (stepThrough (p:path)) trees

takeWhileInclusive :: (a -> Bool) -> [a] -> [a]
takeWhileInclusive _ [] = []
takeWhileInclusive p (x:xs) = x : if p x then takeWhileInclusive p xs else []

{-
Find out what kind of loop a looping path is.
TODO: this is in fact of type Path -> Maybe Loop because not all paths are
loops.
-}

classifyLoop :: Path -> Loop
classifyLoop path = if unlessCount == 0 then Regular path
                         else if even unlessCount then NonMonotonousEven path
                                                  else NonMonotonousOdd path
  where unlessCount = length $ filter snd path

{-
check a loop if it is a bad loop
-}

isBadLoop :: Loop -> Bool
isBadLoop (NonMonotonousOdd _) = True
isBadLoop _ = False

{-
Check many loops if they are all okay
-}

checkLoops :: [Loop] -> Bool
checkLoops = and . map checkLoop
  where checkLoop (NonMonotonousOdd _) = False
        checkLoop _ = True

{-
Currently unused
-}

leafNodes :: ContextTree -> [Premise]
leafNodes (ContextNode val) = [val]
leafNodes (Branch _ trees)  = L.concatMap leafNodes trees
leafNodes (ContextLoop _)   = []

leafLoops :: ContextTree -> [Premise]
leafLoops (ContextLoop val) = [val]
leafLoops (Branch _ trees)  = L.concatMap leafLoops trees
leafLoops (ContextNode _)   = []

elemLoop :: DBNodeId -> Loop -> Bool
elemLoop id' (Regular path)            = L.elem id' $ map fst path
elemLoop id' (NonMonotonousOdd path)   = L.elem id' $ map fst path
elemLoop id' (NonMonotonousEven path)  = L.elem id' $ map fst path


{- TODO: first approach, delete when second approach works
do
  (Entity id' _) <- requireJsonBody :: Handler (Entity DBNode)
  searched <- runDB $ get id'
  case searched of
    Nothing -> error "Node for backward chaining does not exist"
    _ -> do
      graph  <- G.getGraph 
      error $ show $ filter (not.(definedGlobalContext graph)) 
        $ getGlobalContext id' graph
      r <- G.getGraphDB []
      return r

type Path  = [Premise]
data Loop  = Regular Path
           | NonMonotonousOdd  Path
           | NonMonotonousEven Path
  deriving (Show)

instance Eq Loop where
  (Regular x) == (Regular y) = setEqual x y
  (NonMonotonousOdd x) == (NonMonotonousOdd y) = setEqual x y
  (NonMonotonousEven x) == (NonMonotonousEven y) = setEqual x y
  _ == _ = False

setEqual :: Eq a => [a] -> [a] -> Bool
setEqual x y = 
  and $ L.concat $ [map (\e -> L.elem e y) x, map (\e -> L.elem e x) y]

data Context = ContextLoop Loop | ContextNode DBNodeId deriving (Show, Eq)

evalContext :: Graph -> Context -> Graph
evalContext graph context = undefined

getGlobalContext :: TargetId -> Graph -> [[Context]]
getGlobalContext goal graph = let premises = G.getLeftSidesByGoal graph goal
  in case premises of
    [] -> [[ContextNode goal]]
    _  -> do
      premises' <- premises
      return $ L.nub $ L.concat $ ruleContext graph premises' goal

ruleContext :: Graph -> [Premise] -> TargetId -> [[Context]]
ruleContext graph premises goal = do
  premise <- premises
  return $ premiseContext graph premise [premise] goal

premiseContext :: Graph -> Premise -> [Premise] -> TargetId -> [Context]
premiseContext graph premise path goal = 
  let next = L.concat $ G.getLeftSidesByGoal graph $ fst premise
  in case next of
    [] -> [ContextNode $ fst premise]
    _  -> case L.intersect path next of
      []    -> L.concat $ map (\p -> premiseContext graph p (p:path) goal) next
      (x:_) -> let loop = takeWhileInclusive (\p -> p /= x) path
        in if goal == (fst premise)
          then [ContextLoop $ classifyLoop loop]
          else (ContextLoop $ classifyLoop loop):(
            L.concatMap (\p -> premiseContext graph p (p:path) goal) 
                        (L.delete x next))

classifyLoop :: [Premise] -> Loop
classifyLoop path = if unlessCount == 0 then Regular path
                         else if even unlessCount then NonMonotonousEven path
                                                  else NonMonotonousOdd path
  where unlessCount = length $ filter snd path

definedGlobalContext :: Graph -> [Context] -> Bool
definedGlobalContext graph context = and $ 
  map (\c -> definedContext graph c) context

definedContext :: Graph -> Context -> Bool
definedContext graph context = case context of
  ContextLoop loop    -> case loop of
    Regular           path -> definedPath graph path
    NonMonotonousEven path -> definedPath graph path
    _ -> False
  ContextNode node -> definedNode graph node

definedPath :: Graph -> Path -> Bool
definedPath graph path = or $ map (\p -> (definedNode graph $ fst p)) path

definedNode :: Graph -> DBNodeId -> Bool
definedNode graph node = case G.getNodeValue graph node of
  Nothing      -> False
  Just Unknown -> False
  _            -> True

valid :: [Context] -> Bool
valid (c:cs) = case c of
  (ContextLoop (NonMonotonousOdd _)) -> False
  _ -> valid cs
valid [] = True 

findBadLoops :: [Context] -> [Loop]
findBadLoops (c:cs) = case c of
  (ContextLoop (NonMonotonousOdd l)) -> (NonMonotonousOdd l):(findBadLoops cs)
  _ -> findBadLoops cs
findBadLoops [] = [] 

takeWhileInclusive :: (a -> Bool) -> [a] -> [a]
takeWhileInclusive _ [] = []
takeWhileInclusive p (x:xs) = x : if p x then takeWhileInclusive p xs else []

-}
