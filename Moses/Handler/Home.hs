{-# LANGUAGE OverloadedStrings #-}

module Handler.Home where

import Import
import Text.Julius
import Graph.Graph as G

getHomeR :: Handler Html
getHomeR = defaultLayout $ do
            rulerId <- newIdent
            messageRender <- getMessageRender
            graph <- handlerToWidget $ G.getGraphDB []
            let msg = rawJS . messageRender
            setTitle "Moses rulez!"
            
            -- CSS
            ---- Helper
            addStylesheet $ StaticR css_buttons_css  
            addStylesheet $ StaticR css_jqueryui_css  
            
            ---- Own
            addStylesheet $ StaticR css_home_css  
            addStylesheet $ StaticR css_context_css                
            addStylesheet $ StaticR css_info_css                
            addStylesheet $ StaticR css_rwv_css 
            addStylesheet $ StaticR css_ie_css 
            addStylesheet $ StaticR css_nomo_css                            
                                            
            -- JavaScript
            ---- Helper        
            addScript $ StaticR js_d3_js
            addScript $ StaticR js_jquery_js
            addScript $ StaticR js_jqueryui_js
            addScript $ StaticR js_mustache_js
            addScript $ StaticR js_data_js
            addScript $ StaticR js_const_js
            addScript $ StaticR js_helper_js  
            
            ---- Own
            addScript $ StaticR js_graph_js
            addScript $ StaticR js_filter_js
            addScript $ StaticR js_drawPath_js
            addScript $ StaticR js_drag_js
            addScript $ StaticR js_scroll_js
            addScript $ StaticR js_zoom_js
            addScript $ StaticR js_context_js
            addScript $ StaticR js_contextNode_js
            addScript $ StaticR js_contextRule_js
            addScript $ StaticR js_contextCond_js         
            addScript $ StaticR js_contextBg_js    
            addScript $ StaticR js_repaint_js
            addScript $ StaticR js_selection_js
            addScript $ StaticR js_info_js
            addScript $ StaticR js_rwv_js 
            addScript $ StaticR js_ie_js                        
            addScript $ StaticR js_nomo_js                        
            
            $(widgetFile "home") 
