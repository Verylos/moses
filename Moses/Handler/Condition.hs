module Handler.Condition where

import Import
import Prelude (and)
import Control.Monad (forM)
import Graph.Graph as G

postConditionR :: Handler Value
postConditionR = do
  mCond <- requireJsonBody :: Handler (Maybe [Entity DBCondition])
  case mCond of
    Nothing -> invalidArgs ["could not parse list of conditions"]
    Just conds -> do
      invalid <- invalidLinks conds
      if invalid
        then invalidArgs [ "expected rules and/or sources do not exist" ]
        else do 
          _ <- forM conds $ \(Entity id' (DBCondition s r n)) -> 
            runDB $ update id' [ DBConditionSource =. s
                               , DBConditionRuleId =. r
                               , DBConditionNegate =. n ]
          graph <- G.getGraphDB []
          return . toJSON $ graph 
    

putConditionR :: Handler Value
putConditionR = do
  mCond <- requireJsonBody :: Handler (Maybe [Entity DBCondition])
  case mCond of
    Nothing -> invalidArgs ["could not parse list of conditions"]
    Just conds -> do
      invalid <- invalidLinks conds
      if invalid
        then invalidArgs ["expected rules and/or sources do not exist"]
        else do
          _ <- forM conds $ \(Entity _ cond) ->
            runDB $ insert cond
          graph <- G.getGraphDB []
          return . toJSON $ graph


deleteConditionR :: Handler Value
deleteConditionR = do
  mCond <- requireJsonBody :: Handler (Maybe [Entity DBCondition])
  case mCond of
    Nothing -> invalidArgs ["could not parse list of conditions"]
    Just conds -> do
      _ <- forM conds $ \(Entity id' _) -> 
        runDB $ delete id'
      graph <- G.getGraphDB []
      return . toJSON $ graph
    

invalidLinks :: [Entity DBCondition] -> Handler Bool
invalidLinks conds = do
  invalidities <- forM conds $ \(Entity _ (DBCondition source rule _)) -> do
    mNode <- runDB $ get source
    mRule <- runDB $ get rule
    case (mNode, mRule) of
      (Just _, Just _) -> return False
      _                -> return True
  return $ or invalidities
