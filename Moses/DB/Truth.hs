module DB.Truth
  ( Truth(..)
  , andTruth
  , orTruth
  , negateTruth
  )
where

import Data.Aeson.Types as A
import Prelude
import Control.Applicative (pure)
import Database.Persist.TH

data Truth = Pos
           | Unknown
           | Neg
  deriving (Eq, Show, Read, Ord)
derivePersistField "Truth"


andTruth :: [Truth] -> Truth
andTruth = foldl and' Pos

and' :: Truth -> Truth -> Truth
and' a b = case (a, b) of
  (Pos, Pos)         -> Pos
  (Pos, Unknown)     -> Unknown
  (Unknown, Pos)     -> Unknown
  (Unknown, Unknown) -> Unknown
  _                  -> Neg


orTruth :: [Truth] -> Truth
orTruth = foldl or' Neg

or' :: Truth -> Truth -> Truth
or' a b = case (a, b) of
  (Unknown, Unknown) -> Unknown
  (Neg, Unknown)     -> Unknown
  (Unknown, Neg)     -> Unknown
  (Neg, Neg)         -> Neg
  _                  -> Pos

negateTruth :: Truth -> Truth
negateTruth t = case t of
  Pos     -> Neg
  Neg     -> Pos
  Unknown -> Unknown




instance ToJSON Truth where
  toJSON t = case t of
    Pos     -> A.Bool True
    Neg     -> A.Bool False
    Unknown -> A.Null

instance FromJSON Truth where
  parseJSON o = case o of
    A.Bool True   -> pure Pos
    A.Bool False  -> pure Neg
    A.Null        -> pure Unknown
    x             -> error ("Could not parse the Truth: " ++ show x)
