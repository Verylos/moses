module DB.DB
( editNodes
, addNodes
, addRules
, addConditions
, addConclusions
, deleteDB
) where

import Import
import Control.Monad (forM)

{-
DB is a Module to handle database interactions used by the handlers
TODO: not yet complete
-}

editNodes :: [Entity DBNode] -> Handler ()
editNodes nodes = do
  _ <- forM nodes $ \(Entity id' (DBNode name value x y)) ->
    runDB $ update id' [ DBNodeName  =. name
                     , DBNodeValue =. value
                     , DBNodeX     =. x
                     , DBNodeY     =. y ]
  return ()

addNodes :: [Entity DBNode] -> Handler ()
addNodes nodes = do
  _ <- forM nodes $ \(Entity _ node) ->
    runDB $ insert node
  return ()


addRules :: [Entity DBRule] -> Handler ()
addRules rules = do
  _ <- forM rules $ \(Entity _ rule) ->
    runDB . insert $ rule
  return ()

addConditions :: [Entity DBCondition] -> Handler ()
addConditions conds = do
  invalid <- invalidConds conds
  if invalid
    then invalidArgs ["expected rules and/or sources do not exist"]
    else do
      _ <- forM conds $ \(Entity _ cond) ->
        runDB $ insert cond
      return ()
  return ()

addConclusions :: [Entity DBConclusion] -> Handler ()
addConclusions concs = do
  invalid <- invalidConcs concs
  if invalid 
    then invalidArgs ["expected rules and/or targets do not exist"]
    else do
      _ <- forM concs $ \(Entity _ conc) ->
        runDB $ insert conc
      return ()
  return ()

invalidConds :: [Entity DBCondition] -> Handler Bool
invalidConds conds = do
  invalidities <- forM conds $ \(Entity _ (DBCondition source rule _)) -> do
    mNode <- runDB $ get source
    mRule <- runDB $ get rule
    case (mNode, mRule) of
      (Just _, Just _) -> return False
      _                -> return True
  return $ or invalidities

invalidConcs :: [Entity DBConclusion] -> Handler Bool
invalidConcs concs = do
  invalidities <- forM concs $ \(Entity _ (DBConclusion rule target)) -> do
    mRule <- runDB $ get rule
    mNode <- runDB $ get target
    case (mRule, mNode) of
      (Just _, Just _) -> return False
      _                -> return True
  return $ or invalidities

deleteDB :: Handler ()
deleteDB = runDB $ ( do
  deleteWhere ([] :: [Filter DBNode])
  deleteWhere ([] :: [Filter DBRule])
  deleteWhere ([] :: [Filter DBCondition])
  deleteWhere ([] :: [Filter DBConclusion]) )
