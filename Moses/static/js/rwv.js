Rwv = { 
  current: { },
  node: null
};

Rwv.hide = function() {
  $("#" + C.ids.RWV).css({ visibility: "hidden" });
};

Rwv.show = function(variables) {
  var rwv = $("#" + C.ids.RWV);
  rwv.css({ 
    left: $(window).width() / 2 - 300,
    top:  50, visibility: "visible" 
  });  
  
  var header = "<center><span class='bigfont'>" + C.strings.context.RWV 
             + "</span></center><span class='smallfont'>" 
             + C.strings.context.RWV_DESC + "</span>";
  
  Rwv.current = { };
  variables.forEach(function(id) { Rwv.current["" + id] = null; });
  var entries = variables.map(Rwv.createListEntry).join("");   
  
  var buttons = "<center><table><tr><td style='width:50%'>" 
            + "<input type='button' class='button medium gray' "
            + " onClick='Rwv.submit()' "
            + " style='width:100px' value='" + C.strings.context.SUBMIT + "' />"
          + "</td><td style='width:50%'>"  
            + "<input type='button' class='button medium gray' "
            + " onClick='Rwv.cancel()' "
            + " style='width:100px' value='" + C.strings.context.CANCEL + "' />" 
          + "</td></tr></table></center>";
  
  rwv.html(header + "<hr><table style='max-height:400px;overflow:auto;display:block'>" 
                  + entries + "</table><hr>" + buttons);
};

Rwv.start = function(data) {
  Rwv.node = Context.Node.current;
  if(data.messages != null) {
    if(data.messages.ask) {
      Rwv.show(data.messages.ask);  
    } else {
      NoMo.show(data.messages.loops[0].NonMonotonousOdd);
    }    
  } else {
    Graph.updateGraph(data);
  }
};

Rwv.idToNode = function(id, value) {
  var node = $("#" + C.ids.NODE_ID + id);
  var nodeData = Graph.getNodeById(id);
  return {
    id:    parseInt(id), name: nodeData.name,
    value: value, x: nodeData.x, y: nodeData.y    
  };
};

Rwv.cancel = function() {
  Rwv.hide();
  Rwv.current = { };
};

Rwv.submit = function() {
  Rwv.hide();
  var nodes = [];
  for(var id in Rwv.current) {
    nodes.push(Rwv.idToNode(id, Rwv.current[id]));
  }
  return Context.db2("POST", C.routes.NODE, nodes, function(data) {
    Graph.updateGraph(data);
    return Context.db2("POST", C.routes.RWV, Rwv.node, Rwv.start);
  });  
};

Rwv.createListEntry = function(index) {
  var name = $("#" + C.ids.NODE_ID + index).children("text").text();
  var result = "<tr>";
  result += "<td style='width:35%'><input type='button' "
            + " id='listEntry" + index + "'"
            + " class='button blue' style='width: 100%' "
            + " value='" + name + "' /></td>"; 
  result += "<td style='width:5%'></td>";             
  result += "<td style='width:20%'><input type='button' "
            + " onClick='Rwv.setValue(" + index + ", true)' " 
            + " class='button medium green' style='width: 100%' "
            + " value='" + C.strings.TRUE + "' /></td>";
  result += "<td style='width:20%'><input type='button' "
            + " onClick='Rwv.setValue(" + index + ", null)' " 
            + " class='button medium blue' style='width: 100%' "
            + " value='" + C.strings.UNKNOWN + "' /></td>";
  result += "<td style='width:20%'><input type='button' "
            + " onClick='Rwv.setValue(" + index + ", false)' " 
            + " class='button medium red' style='width: 100%' "
            + " value='" + C.strings.FALSE + "' /></td>";                        
  return  result + "</tr>";
};

Rwv.setValue = function(id, value) {
  Rwv.current["" + id] = value;
  $("#listEntry" + id).attr("class", "button " + Helper.valueToColor(value));
};
