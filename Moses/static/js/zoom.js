Zoom = { MIN: 0.5, MAX: 3 };

Zoom.zoom = function() {
  Graph.display.transition().duration(100).ease("linear")
      .attr("transform", "translate(" + d3.event.translate + ")scale(" 
          + d3.event.scale + ")");    
};

Zoom.listener = d3.behavior.zoom()
    .scaleExtent([Zoom.MIN, Zoom.MAX]).on("zoom", Zoom.zoom);

Graph.svg.call(Zoom.listener);

Zoom.transform = function() {
  return d3.transform(Graph.display.attr("transform"));
};

Zoom.scale = function() {
  return Zoom.transform().scale;
};

Zoom.translate = function() {
  return Zoom.transform().translate;
};
