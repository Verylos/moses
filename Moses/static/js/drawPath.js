Graph.drawCond = function(cond) {
  var node = Graph.nodeData.find(function(n) { 
          return Data.id(n) == Data.source(cond); }),
      rule = Graph.ruleData.find(function(r) {
          return Data.id(r) == Data.rule(cond);   });
  var source = { x: node.x + Helper.nodeWidth(node)/2, y: node.y },
      target = { x: rule.x - C.nums.RULE_SIZE,         y: rule.y };
  return Graph.drawPath(source, target);
};  

Graph.drawConc = function(conc) {
  var node = Graph.nodeData.find(function(n) { 
          return Data.id(n) == Data.target(conc); }),
      rule = Graph.ruleData.find(function(r) {
          return Data.id(r) == Data.rule(conc);   });
  var source = { x: rule.x + C.nums.RULE_SIZE,         y: rule.y },
      target = { x: node.x - Helper.nodeWidth(node)/2, y: node.y };
  return Graph.drawPath(source, target);
};  

Graph.drawPath = function(source, target) {
  var diff   = { x: target.x - source.x, y: target.y - source.y },  
      off    = source.x > target.x ? C.nums.PATH_OFF :
                  max(abs(diff.x/4), C.nums.PATH_OFF),
      preSource = { x: source.x + off, y: source.y },
      preTarget = { x: target.x - off, y: target.y },      
      preDiff   = { x: preTarget.x-preSource.x, y: preTarget.y-preSource.y },
      data = [ source, preSource ];
      
  if(preSource.x > preTarget.x) { 
    data.push({ x: preSource.x, y: preSource.y + preDiff.y/3 });
    data.push({ x: preTarget.x, y: preTarget.y - preDiff.y/3 });
  } else {
    data.push({ x: preSource.x + preDiff.x/3, y: preSource.y });
    data.push({ x: preTarget.x - preDiff.x/3, y: preTarget.y });
  }        
  data.push(preTarget);
  data.push(target);
  return Data.line.func(data);
};
