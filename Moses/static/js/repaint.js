Graph.repaint = function() {
  // Update data
  Graph.nodes = Graph.nodes.data(Graph.nodeData, Data.id);
  Graph.rules = Graph.rules.data(Graph.ruleData, Data.id);
  Graph.conds = Graph.conds.data(Graph.condData, Data.id);
  Graph.concs = Graph.concs.data(Graph.concData, Data.id);
      
  Graph.addNew();
  Graph.updateExisting();
  Graph.removeExit(); 
};

Graph.addNew = function() {
  // Create new Node's Group
  var newNode = Graph.nodes.enter().append("g").attr({ 
    id: function(d) { return C.ids.NODE_ID + Data.id(d); },
    transform: function(d) { 
          return "translate(" + Data.x(d) + "," + Data.y(d) + ")"; }
  })        
  .on("contextmenu", Context.Node.show)
  .on("click", Selection.select)
  .on("mousedown", Helper.changeCursor(C.ids.NODE_ID, "move"))
  .on("mouseup",   Helper.changeCursor(C.ids.NODE_ID, "inherit"))
  .call(Drag.listener);  
  
  // Add a Rectangle and Label to the new Group
  newNode.append("rect").attr({ 
    stroke: C.colors.NODE_STROKE,
    x:      function(d) { return 0 - Helper.nodeWidth(d)  / 2; },
    y:      function(d) { return 0 - Helper.nodeHeight(d) / 2; },
    rx:     function(d) { return Helper.nodeHeight(d) / 3; },
    ry:     function(d) { return Helper.nodeHeight(d) / 3; },      
    width:  Helper.nodeWidth, 
    height: Helper.nodeHeight,
    fill:   C.colors.NODE
  });
  newNode.append("text").text(Data.name).attr({
    fill: C.colors.TEXT_STROKE,
    x: function(d) { return 0 - Data.name(d).escape().vLength() / 2; },
    y: function(d) { return 0 + Data.name(d).escape().vHeight() / 2 
          - C.nums.NODE_VER(Data.name(d).escape().vHeight()); }
  });  
  
  var newRule = Graph.rules.enter().append("circle").attr({ 
    id: function(d) { return C.ids.RULE_ID + Data.id(d); }, 
    transform: function(d) { 
          return "translate(" + Data.x(d) + "," + Data.y(d) + ")"; }
  })        
  .on("contextmenu", Context.Rule.show)
  .on("mousedown", Helper.changeCursor(C.ids.RULE_ID, "move"))
  .on("mouseup",   Helper.changeCursor(C.ids.RULE_ID, "inherit"))
  .call(Drag.listener);  
        
  var newCond = Graph.conds.enter().append("path").attr({
    id: function(d) { return C.ids.COND_ID + Data.id(d); },
    "stroke-width": 2,
    fill: "none",
    d: Graph.drawCond,
    stroke: function(d) { return Data.negate(d) ? 
            C.colors.COND_STROKE_NEG : C.colors.COND_STROKE; }
  })
  .style('marker-end', 'url(#end-arrow)')
  .on("mouseover", Helper.changeCursor(C.ids.COND_ID, "pointer"))
  .on("contextmenu", function(d) { Context.Cond.show(d); });
    
  var newConc = Graph.concs.enter().append("path").attr({
    id: function(d) { return C.ids.CONC_ID + Data.id(d); },
    stroke: C.colors.CONC_STROKE,
    "stroke-width": 2,
    fill: "none",
    d: Graph.drawConc
  })
  .style('marker-end', 'url(#end-arrow)');   
};

// Update all Components
Graph.updateExisting = function() {
  
  // Update all Nodes
  Graph.nodeData.forEach( function(d) {
  
    // Update Node's Rectangle
    d3.select("#" + C.ids.NODE_ID + Data.id(d)).selectAll("rect").attr({ 
      stroke: C.colors.NODE_STROKE,
      x:      0 - Helper.nodeWidth(d)  / 2,
      y:      0 - Helper.nodeHeight(d) / 2,
      rx:     Helper.nodeHeight(d) / 3,
      ry:     Helper.nodeHeight(d) / 3,      
      width:  Helper.nodeWidth(d), 
      height: Helper.nodeHeight(d)
    })
    .transition().duration(500)
    .attr({ fill: C.colors.NODE(d) });
  
    // Update Node's Text  
    d3.select("#" + C.ids.NODE_ID + Data.id(d)).selectAll("text")
      .text(Data.name(d)).attr({
        fill: C.colors.TEXT_STROKE,
        x: 0 - Data.name(d).escape().vLength() / 2,
        y: 0 + Data.name(d).escape().vHeight() / 2 
              - C.nums.NODE_VER(Data.name(d).escape().vHeight())
      })
      .style({
        font: "Arial, Helvetica, sans-serif",
        'text-shadow': "0 1px 1px rgba(0,0,0,.3)",
        'text-decoration': "none"
      });
  });
  
  // Update all Nodes
  Graph.ruleData.forEach( function(d) {  
    // Update Node's Rectangle
    d3.select("#" + C.ids.RULE_ID + Data.id(d)).attr({ 
      stroke: C.colors.RULE_STROKE,
      fill:   C.colors.RULE,
      r:      C.nums.RULE_SIZE,
      cx:     0,     
      cy:     0        
    });
  });
  
  // Update all Conditions
  Graph.condData.forEach( function(c) {
    d3.select("#" + C.ids.COND_ID + Data.id(c)).attr({ d: Graph.drawCond })
      .transition().duration(1000)
      .attr({ stroke: function(d) { return Data.negate(d) ? 
            C.colors.COND_STROKE_NEG : C.colors.COND_STROKE; } });
  });  
  
  // Update all Conclusions
  Graph.concData.forEach( function(c) {
    d3.select("#" + C.ids.CONC_ID + Data.id(c)).attr({ d: Graph.drawConc });
  });  
};

Graph.removeExit = function() {
  // Remove Rectangles  
  Graph.nodes.exit().selectAll("rect")
    .transition().duration(1000).style("opacity", 0).remove();     
    
  // Remove Labels  
  Graph.nodes.exit().selectAll("text")
    .transition().duration(1000).style("opacity", 0).remove(); 
    
  // Remove Rules  
  Graph.rules.exit()
    .transition().duration(1000).style("opacity", 0).remove();      
    
  // Remove Conditions  
  Graph.conds.exit()
    .transition().duration(1000).style("opacity", 0).remove();  
    
  // Remove Conclusions
  Graph.concs.exit()
    .transition().duration(1000).style("opacity", 0).remove();         
};
