Context = { };

Context.hide = function() {
  $("#" + C.ids.CONTEXT).css({ visibility: "hidden" });
};
$("#" + C.ids.GRAPH_DIV).bind("mousedown", Context.hide);

Context.show = function(html, environment) {
  // Supress browser's default context menu
  var event = d3.event;  
  event.preventDefault();    
  Drag.active = false;
  
  if(!Selection.active) {
    Info.hide();
    $.get(html, function(template) { 
      var context = $("#" + C.ids.CONTEXT);  
      context.html(Mustache.render(template, environment));
      context.css({ 
        left: event.clientX, 
        top:  event.clientY, 
        visibility: "visible" 
      });
    });  
  }  
  event.stopPropagation();
};

Context.db2 = function(mode, route, objects, func) {
  Context.hide();
  $.ajax(route, {
    type: mode, success: func,
    data: JSON.stringify(objects)
  });
  return false;
};

Context.db = function(mode, route, objects) {
  return Context.db2(mode, route, objects, Graph.updateGraph);
};
