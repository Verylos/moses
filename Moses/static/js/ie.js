IE = { };

IE.hide = function() {
  $("#" + C.ids.IE).css({ visibility: "hidden" });
};

IE.show = function() {
  var ie = $("#" + C.ids.IE);
  ie.css({ 
    left: $(window).width() / 2 - 200,
    top:  50, visibility: "visible" 
  });  
  
  var buttons = "<center>"
            + "<input type='button' class='button medium gray' "
            + " onClick='IE.import()' "
            + " style='width:100px' value='" + C.strings.context.IMPORT + "' />"
            + "<input type='button' class='button medium gray' "
            + " onClick='IE.hide()' "
            + " style='width:100px' value='" + C.strings.context.CANCEL + "' />" 
            + "<input type='button' class='button medium gray' "
            + " onClick='IE.export()' "
            + " style='width:100px' value='" + C.strings.context.EXPORT + "' />"
          + "</center><div style='height:10px' />";
  
  var textField = "<textarea id='iearea' name='ieArea'></textarea>";
  
  var deleteButton = "<center><input type='button' class='button medium black' "
            + " onClick='IE.deleteG()' value='" + C.strings.context.DELETE_GRAPH 
            + "' /></center>"
  
  ie.html(buttons + textField + deleteButton);
};

IE.export = function() {
  Context.db2("POST", C.routes.EXPORT, [], function(data) { 
    $('#iearea').val(JSON.stringify(data));
    setTimeout(function() { $('#iearea').focus(); }, 100);
  });
};

IE.import = function() {
  Context.db2("POST", C.routes.IMPORT, JSON.parse($('#iearea').val()),
    function(data) {
      Graph.updateGraph(data);
      if(data.messages == null) {
        IE.hide();
      } else {
        Log(data.messages);
      }
    });
    
};

IE.deleteG = function() {
  Context.db("POST", C.routes.IMPORT, { rules: [], conds: [], 
                                                   concs: [], nodes: [] });
  IE.hide();                                                   
};
