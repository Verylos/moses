C = {

  routes: {
    NODE:   "/node",
    RULE:   "/rule",
    RESET:  "/reset",
    VWV:    "/vwv",
    RWV:    "/rwv",
    COND:   "/cond",
    CONC:   "/conc",
    IMPORT: "/import",
    EXPORT: "/export"
  },

  strings: { // these strings shall later be overriden by interpolation    
    TRUE:    "<todo>", 
    UNKNOWN: "<todo>", 
    FALSE:   "<todo>",
        
    context: {
      SUBMIT: "<todo>", 
      CANCEL: "<todo>", 
      DELETE: "<todo>",
      NEGATE: "<todo>",
      
      CONDS: "<todo>",
      CONCS: "<todo>",
      
      SEL_DESC: "<todo>",
      
      NEW_VAR:      "<todo>",
      NEW_VAR_TEXT: "<todo>",
      NEW_RULE:     "<todo>",
      RESET:        "<todo>",
      VWV:          "<todo>",
      RWV:          "<todo>",
      IE:           "<todo>",
      RWV_DESC:     "<todo>",
      
      NOMO_HEAD:    "<todo>",
      NOMO_DESC:    "<todo>",
      DELETE_GRAPH: "<todo>",
      
      NEW_VAR_INFO:        "<todo>",
      NEW_VAR_INFO_LONG:   "<todo>",
      NEW_RULE_INFO:       "<todo>",
      NEW_RULE_INFO_LONG:  "<todo",
      RESET_INFO:          "<todo>",
      RESET_INFO_LONG:     "<todo>",
      VWV_INFO:            "<todo>",
      VWV_INFO_LONG:       "<todo>",
      RWV_INFO:            "<todo>",
      RWV_INFO_LONG:       "<todo>",
      IE_INFO:             "<todo>",
      IE_INFO_LONG:        "<todo>",
      SET_CONDS_INFO:      "<todo>",
      SET_CONDS_INFO_LONG: "<todo>",
      SET_CONCS_INFO:      "<todo>",
      SET_CONCS_INFO_LONG: "<todo>",
      NEGATE_INFO:         "<todo>",
      NEGATE_INFO_LONG:    "<todo>"
    }
  },

  colors: {
    TRUE:    "green", 
    UNKNOWN: "blue", 
    FALSE:   "red",
    
    TRUE_RGB:    "#0b0",//"#64991e", 
    UNKNOWN_RGB: "#0095cd", 
    FALSE_RGB:   "#d00",//"#d81b21",
    
    NODE: function(d) {  
      var value = Data.value(d);
      return value == null ? C.colors.UNKNOWN_RGB 
                           : (value ? C.colors.TRUE_RGB : C.colors.FALSE_RGB);
    },
    RULE: "#888",
    
    NODE_STROKE: "#777",
    RULE_STROKE: "#777",
    TEXT_STROKE: "#fff",
    COND_STROKE: "#000",
    COND_STROKE_NEG: "#f00",
    CONC_STROKE: "#00f",    
        
    context: {
      CANCEL: "gray",
      DELETE: "black",
      SUBMIT: "gray",
      NEGATE: "gray",
      
      CONDS: "gray",
      CONCS: "gray",
    
      NEW_VAR:      "gray",
      NEW_VAR_TEXT: "white",
      NEW_RULE:     "gray",
      RESET:        "gray",
      VWV:          "gray",
      RWV:          "gray",
      IE:           "gray" 
    }
  },
  
  ids: {
    RULER:     "ruler",
    GRAPH_DIV: "graphDiv",
    GRAPH:     "graph",
    DISPLAY:   "display",
    CONTEXT:   "context",
    INFO:      "info",
    RWV:       "rwv",
    IE:        "ie",
    NOMO:      "nomo",
    
    SELECTION: "selectionMenu",
    DROP_SHADOW: "dropShadow",
    
    NODES: "nodes",
    RULES: "rules",
    CONDS: "conds",
    CONCS: "concs",
    
    NODE_ID: "node",
    RULE_ID: "rule",
    COND_ID: "cond",
    CONC_ID: "conc",
    
    SCROLL_LEFT:  "scrollLeft",
    SCROLL_RIGHT: "scrollRight",
    SCROLL_DOWN:  "scrollDown",
    SCROLL_UP:    "scrollUp",
    
    context: {
      NAME:  "context_name",
      VALUE: "context_value",
      ID:    "context_id",
      X:     "context_x",
      Y:     "context_y",
      
      NEW_NAME: "context_newname",
      SOURCE:   "context_source"
    }    
  },
  
  nums: {
    NODE_HOR: function(size) { return size / 3 },
    NODE_VER: function(size) { return size / 5 },
     
    RULE_SIZE: 10,
    PATH_OFF:  20
  },
  
  selection: {
    MODE_COND: 0,
    MODE_CONC: 1,
    MODE_VWV:  2    
  },
  
  html: {
    CONTEXT_NODE: "static/html/contextNode.html",
    CONTEXT_RULE: "static/html/contextRule.html",
    CONTEXT_COND: "static/html/contextCond.html",    
    CONTEXT_BG:   "static/html/contextBg.html",
    SELECTION:    "static/html/selection.html"
  }

};
