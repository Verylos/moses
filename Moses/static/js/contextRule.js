Context.Rule = {
  sizes: {
    CONDS:  "medium", 
    CONCS:  "medium",
    DELETE: "medium"
  },
  current: null  
};

Context.Rule.show = function(rule) {
  Context.Rule.current = rule;
  Context.show(C.html.CONTEXT_RULE, {
    sizeConds:   Context.Rule.sizes.CONDS,  
    stringConds: C.strings.context.CONDS, 
    colorConds:  C.colors.context.CONDS,
    
    sizeConcs:   Context.Rule.sizes.CONCS,
    stringConcs: C.strings.context.CONCS, 
    colorConcs:  C.colors.context.CONCS, 
    
    sizeDelete:   Context.Rule.sizes.DELETE, 
    stringDelete: C.strings.context.DELETE,
    colorDelete:  C.colors.context.DELETE,
    
    infoSetConds: C.strings.context.SET_CONDS_INFO,
    infoSetConcs: C.strings.context.SET_CONCS_INFO,
    
    infoSetCondsLong: C.strings.context.SET_CONDS_INFO_LONG,
    infoSetConcsLong: C.strings.context.SET_CONCS_INFO_LONG
  });
};

Context.Rule.setConds = function() {
  Context.hide();      
  Selection.show(Data.id(Context.Rule.current), 
      C.routes.COND, C.selection.MODE_COND);
};

Context.Rule.setConcs = function() {
  Context.hide();      
  Selection.show(Data.id(Context.Rule.current),
      C.routes.CONC, C.selection.MODE_CONC);  
};

Context.Rule.delete = function() {
  return Context.db("DELETE", C.routes.RULE, [Context.Rule.current]);
};
