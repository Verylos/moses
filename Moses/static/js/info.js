Info = { 
  ATTRIBUTE: "title"
};

Info.hide = function() {
  $("#" + C.ids.INFO).css({ visibility: "hidden" });
};
$("#" + C.ids.GRAPH_DIV).bind("mousedown", Info.hide);

$(function() {
  $(document).tooltip({
    items: "[" + Info.ATTRIBUTE + "]",
    content: function() {
      return "<span class='smallfont'>" + $(this).prop(Info.ATTRIBUTE) + "</span>";   
    }
  });
});

Info.showLong = function(html) {
  info = $("#" + C.ids.INFO);
  info.html(html);
  info.css({
    left: 10, 
    top:  10, 
    visibility: "visible" 
  });
  return false;
};
