Context.Node = {
  sizes: {
    NAME:   "medium", 
    ACTION: "medium",
    VALUE:  "medium",
    RWV:    "medium"
  },
  current: null,
  value:   null  
};

Context.Node.show = function(node) {  
  Context.Node.current = node;
  Context.Node.value = Data.value(node); 
  
  Context.show(C.html.CONTEXT_NODE, {    
    valueColor: Helper.valueToColor(Data.value(node)),     
    
    idName: C.ids.context.NAME,
    name:   Data.name(Context.Node.current),  
    
    sizeName:   Context.Node.sizes.NAME,    
    sizeAction: Context.Node.sizes.ACTION, 
    sizeValue:  Context.Node.sizes.VALUE, 
    sizeRwv:    Context.Node.sizes.RWV,        
    
    stringCancel: C.strings.context.CANCEL,       
    stringDelete: C.strings.context.DELETE,      
    stringSubmit: C.strings.context.SUBMIT,
    stringRwv:    C.strings.context.RWV, 
    
    infoRwv:      C.strings.context.RWV_INFO,
    infoRwvLong:  C.strings.context.RWV_INFO_LONG,   
    
    colorCancel: C.colors.context.CANCEL,
    colorDelete: C.colors.context.DELETE, 
    colorSubmit: C.colors.context.SUBMIT,
    colorRwv:    C.colors.context.RWV,  
    
    trueColor:    C.colors.TRUE,    trueString:    C.strings.TRUE,      
    unknownColor: C.colors.UNKNOWN, unknownString: C.strings.UNKNOWN,     
    falseColor:   C.colors.FALSE,   falseString:   C.strings.FALSE
  });
};      

Context.Node.setValue = function(value) {
  $("#" + C.ids.context.NAME).attr("class", "button " + Context.Node.sizes.NAME
        + " " + Helper.valueToColor(value));
  Context.Node.value = value;
};

Context.Node.getEdited = function() {
  return {
    id:    Data.id(Context.Node.current),
    x:     Data.x(Context.Node.current),
    y:     Data.y(Context.Node.current),
    name:  $("#" + C.ids.context.NAME).val(),
    value: Context.Node.value
  };
};

Context.Node.rwv = function() {
  return Context.db2("POST", C.routes.RWV, Context.Node.current, Rwv.start);
};

Context.Node.submit = function() {
  return Context.db("POST", C.routes.NODE, [Context.Node.getEdited()]);
};

Context.Node.delete = function() {
  return Context.db("DELETE", C.routes.NODE, [Context.Node.current]);
};
