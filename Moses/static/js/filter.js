// Drop shadow filter
Graph.defs = Graph.svg.append("defs");
Graph.dropFilter = Graph.defs.append("filter")
  .attr({ 
    id: C.ids.DROP_SHADOW, 
    x: "-100%",
    y: "-100%",
    height: "400%", 
    width:  "400%" 
  });
  
Graph.dropFilter.append("feOffset")
  .attr({
    result: "offOut",
    in: "SourceGraphic",
    dx: 0,
    dy: 0
  });  
Graph.dropFilter.append("feColorMatrix")
  .attr({
    result: "matrixOut",
    in: "offOut",
    type: "matrix",
    values: "0 0 0 0 0, 0 0 0 0 0, 0 0 0 0 0, 0 0 0 1 0"
  });    
Graph.dropFilter.append("feGaussianBlur")
  .attr({
    result: "blurOut",
    in: "matrixOut",
    stdDeviation: 7
  });
Graph.dropFilter.append("feBlend")
  .attr({
    in: "SourceGraphic",
    in2: "blurOut",
    mode: "normal"
  });
