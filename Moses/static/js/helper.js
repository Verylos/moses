String.prototype.vLength = function() {
  var ruler = document.getElementById(C.ids.RULER);
  ruler.innerHTML = this;
  return ruler.offsetWidth;
};

String.prototype.vHeight = function() {
  var ruler = document.getElementById(C.ids.RULER);
  ruler.innerHTML = this;
  var style = window.getComputedStyle(ruler, null).getPropertyValue("font-size");
  return parseFloat(style.substring(0, style.length - 2));
};

String.prototype.escape = function() {
  var chr = {
    '"': '&quot;', '&': '&amp;', "'": '&#39;',
    '/': '&#47;', '<': '&lt;', '>': '&gt;'
  };
  return this.replace(/[\"&'\/<>]/g, function(a) { return chr[a]; });
};

Array.prototype.intersect = function(other) {
  var a = this, b = other;
  if(other.length > this.length) {
    a = other; b = this; 
  } return a.filter(function(e) { return b.indexOf(e) !== -1; });
};

Array.prototype.diff = function(a) {
  return this.filter(function(i) { return a.indexOf(i) < 0; });
};

Array.prototype.contains = function(elem) {
  for(var i = 0; i < this.length; i++) {
    if(this[i] == elem) { return true; }
  } return false;
};

Array.prototype.remove = function(elem) {
  return this.filter(function(e) { return e != elem; });
};

Log = function(d) {
  console.log(JSON.stringify(d));
};

abs = function(num) {
  return num < 0 ? -num : num;
}

max = function(a,b) {
  return a < b ? b : a;
};

min = function(a,b) {
  return a > b ? b : a;
};

Helper = { };

Helper.nodeWidth = function(d) { 
  var length = Data.name(d).escape().vLength(),
      height = Data.name(d).escape().vHeight();
  return length + 2 * C.nums.NODE_HOR(height); 
};

Helper.nodeHeight = function(d) { 
  var height = Data.name(d).escape().vHeight();
  return height + 2 * C.nums.NODE_VER(height); 
};

Helper.valueToColor = function(value) {
  return value == null ? C.colors.UNKNOWN :
      (value ? C.colors.TRUE : C.colors.FALSE);
};

Helper.stringToValue = function(s) {
  return s == "null" ? null : (s == "true" ? true : false);
};

Helper.changeCursor = function(prefix, cursor) {
  return function(d) {
    document.querySelector("#" + prefix + Data.id(d)).style.cursor = cursor;
  };
};
