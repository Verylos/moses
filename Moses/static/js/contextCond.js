Context.Cond = {
  sizes: {
    NEGATE: "medium", 
    DELETE: "medium"
  },  
  current: null
};

Context.Cond.show = function(d) {
  Context.Cond.current = d;
  Context.show(C.html.CONTEXT_COND, {  
    sizeNegate:   Context.Cond.sizes.NEGATE,     
    colorNegate:  C.colors.context.NEGATE,
    stringNegate: C.strings.context.NEGATE,
            
    sizeDelete:   Context.Cond.sizes.DELETE,     
    colorDelete:  C.colors.context.DELETE,
    stringDelete: C.strings.context.DELETE,
    
    infoNegate:     C.strings.context.NEGATE_INFO,
    infoNegateLong: C.strings.context.NEGATE_INFO_LONG
  });
};

Context.Cond.delete = function() {
  return Context.db("DELETE", C.routes.COND, [Context.Cond.current]);
};

Context.Cond.negate = function() {
  Data.toggleNegate(Context.Cond.current);
  return Context.db("POST", C.routes.COND, [Context.Cond.current]);
};
