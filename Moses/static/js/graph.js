Graph = {
  width:  "100%",
  height: $(window).height() - 5,
  
  nodeData: [],
  ruleData: [],
  condData: [],
  concData: []
};

Graph.svg = d3.select("#" + C.ids.GRAPH_DIV).append("svg")
  .attr({
    id:     C.ids.GRAPH,
    width:  Graph.width,
    height: Graph.height
  });
  
d3.select("#" + C.ids.GRAPH).on("mousedown", function() { 
  document.querySelector("#" + C.ids.GRAPH).style.cursor = "move";
});

d3.select("#" + C.ids.GRAPH).on("mouseup", function() { 
  document.querySelector("#" + C.ids.GRAPH).style.cursor = "inherit";
});    

Graph.display = Graph.svg.append("g")  // Contains all things that can be
    .attr({ id: C.ids.DISPLAY });      // zoomed and moved

Graph.rules = Graph.display.append("svg:g")
      .attr({ id: C.ids.RULES }).selectAll("circle");
Graph.nodes = Graph.display.append("svg:g")
      .attr({ id: C.ids.NODES }).selectAll("rect");
Graph.conds = Graph.display.append("svg:g")
      .attr({ id: C.ids.CONDS }).selectAll("path");
Graph.concs = Graph.display.append("svg:g")
      .attr({ id: C.ids.CONCS }).selectAll("path");

Graph.svg.append('svg:defs').append('svg:marker')
  .attr({ id: 'end-arrow', viewBox: '0 -5 10 10'
        , refX: 6, orient: 'auto', markerWidth: 4, markerHeight: 4 })
    .append('svg:path').attr({ d: 'M0,-5L10,0L0,5', fill: '#000' });

Graph.updateGraph = function(data) {
  Graph.nodeData = data.nodes;
  Graph.ruleData = data.rules;
  Graph.condData = data.conds;
  Graph.concData = data.concs;  
  Graph.repaint();  
};

Graph.getNodeById = function(id) {
  return Graph.nodeData.filter(function(node) { return node.id == id; })[0];
};
