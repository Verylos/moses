Selection = { 
  active: false,
  current: null,
  
  sizes: {
    CANCEL: "medium",
    SUBMIT: "medium"
  }
};

Selection.hide = function() {
  Selection.active = false;
  Selection.current.selected.forEach(Selection.deactivate);
  Selection.current = null;
  $("#" + C.ids.SELECTION).css({ visibility: "hidden" });
};

Selection.activate = function(nodeId) {
  d3.select("#" + C.ids.NODE_ID + nodeId).selectAll("rect")
    .attr({ filter: "url(#" + C.ids.DROP_SHADOW + ")" });
};

Selection.deactivate = function(nodeId) {
  d3.select("#" + C.ids.NODE_ID + nodeId).selectAll("rect")
    .attr({ filter: "" });
};

Selection.show = function(ruleId, route, mode) {
  Selection.active = true;
  Selection.current = { ruleId: ruleId, route: route, mode: mode };
  Info.hide();
  
  switch (Selection.current.mode) {
    case C.selection.MODE_COND:
    case C.selection.MODE_CONC:
      var node = Selection.current.mode == C.selection.MODE_COND
            ? Data.source : Data.target;
      Selection.current.selected = Selection.connected().map(node);
      Selection.current.selected.forEach(Selection.activate);
      break;
    default: Selection.current.selected = [];  
  }
  
  $.get(C.html.SELECTION, function(template) { 
    var environment = {           
      sizeCancel:   Selection.sizes.CANCEL,  
      stringCancel: C.strings.context.CANCEL, 
      colorCancel:  C.colors.context.CANCEL,
      
      sizeSubmit:   Selection.sizes.SUBMIT,
      stringSubmit: C.strings.context.SUBMIT, 
      colorSubmit:  C.colors.context.SUBMIT
    },
    selection = $("#" + C.ids.SELECTION);
    selection.html(Mustache.render(template, environment));
    $("#selDesc").html(C.strings.context.SEL_DESC);
    selection.css({ left: 10, top: 10, visibility: "visible" });
  });
};

Selection.select = function(node) {
  if(Selection.active) {  
    Selection.toggle(Data.id(node));
  }
};

Selection.toggle = function(nodeId) {
  if(Selection.current.selected.contains(nodeId)) {
    // Already selected
    Selection.deactivate(nodeId);
    Selection.current.selected = Selection.current.selected.remove(nodeId);
  } else {    
    // Not selected yet
    Selection.activate(nodeId);
    switch (Selection.current.mode) {
      case C.selection.MODE_COND:
      case C.selection.MODE_VWV:
        Selection.current.selected.push(nodeId); break;        
      case C.selection.MODE_CONC:
        Selection.current.selected.forEach(Selection.deactivate);
        Selection.current.selected = [nodeId];  
    }    
  }
};

Selection.connected = function() {
  switch (Selection.current.mode) {
    case C.selection.MODE_COND: var data = Graph.condData; break;
    case C.selection.MODE_CONC: var data = Graph.concData; break;
    default: var data = [];
  }      
  return data.filter(function(c) { 
    return Data.rule(c) == Selection.current.ruleId; });
};

Selection.submit = function() {
  switch (Selection.current.mode) {
    case C.selection.MODE_COND: return Selection.changeEdited(Data.source); 
    case C.selection.MODE_CONC: return Selection.changeEdited(Data.target); 
    case C.selection.MODE_VWV:
      return Selection.postSelected(Selection.current.route); 
  }         
};

Selection.postSelected = function(route) {
  Context.db("POST", route,
      Selection.current.selected.map(Selection.nodeFromId));
  Selection.hide();
};

Selection.changeEdited = function(node) {
  var connected = Selection.connected().map(node),
      intersect = Selection.current.selected.intersect(connected),
      toDelete  = connected.diff(intersect),
      toCreate  = Selection.current.selected.diff(intersect);
  $.ajax(Selection.current.route, {
    type: "DELETE", data: JSON.stringify(toDelete.map(Selection.fromJSON)),
    success: function(data,bla,blub) {
      Graph.updateGraph(data,bla,blub);
      if(toCreate.length > 0) {
        $.ajax(Selection.current.route, {
          type: "PUT", success: Graph.updateGraph,
          data: JSON.stringify(toCreate.map(Selection.createJSON)) });
      }
      Selection.hide();
    }
  });
};

Selection.nodeFromId = function(id) {
  return Graph.nodeData.filter(function(node) { return Data.id(node) == id; });
};

Selection.createJSON = function(nodeId) {
  if(Selection.current.mode == C.selection.MODE_COND) {
    return { id: 0, source: nodeId, ruleId: Selection.current.ruleId,
        negate: false };
  } else if(Selection.current.mode == C.selection.MODE_CONC) {
    return { id: 0, ruleId: Selection.current.ruleId, target: nodeId };
  }
};

Selection.fromJSON = function(nodeId) {
  switch (Selection.current.mode) {
    case C.selection.MODE_COND:
      return Graph.condData.find(function(c) { return Data.source(c) == nodeId 
              && Data.rule(c) == Selection.current.ruleId; });
    case C.selection.MODE_CONC:
      return Graph.concData.find(function(c) { return Data.target(c) == nodeId 
              && Data.rule(c) == Selection.current.ruleId; });  
  }
};
