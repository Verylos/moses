Context.Bg = {
  sizes: {
    NEW_VAR:  "medium",
    NEW_RULE: "medium",
    RESET:    "medium",
    VWV:      "medium",
    RWV:      "medium",
    IE:       "medium"
  },
  
  eventX:  null,
  eventY:  null
};

Context.Bg.show = function() { 
  Context.Bg.eventX  = d3.event.clientX;
  Context.Bg.eventY  = d3.event.clientY;
  
  Context.show(C.html.CONTEXT_BG, {       
    idName:  C.ids.context.NEW_NAME,        
    
    sizeNewVar:  Context.Bg.sizes.NEW_VAR, 
    sizeNewRule: Context.Bg.sizes.NEW_RULE,
    sizeReset:   Context.Bg.sizes.RESET, 
    sizeVwv:     Context.Bg.sizes.VWV,   
    sizeRwv:     Context.Bg.sizes.RWV,
    sizeIE:      Context.Bg.sizes.IE, 
    
    stringNewVar:   C.strings.context.NEW_VAR,       
    colorNewVar:    C.colors.context.NEW_VAR,  
    infoNewVar:     C.strings.context.NEW_VAR_INFO,
    infoNewVarLong: C.strings.context.NEW_VAR_INFO_LONG,
    
    stringNewRule:   C.strings.context.NEW_RULE,       
    colorNewRule:    C.colors.context.NEW_RULE,   
    infoNewRule:     C.strings.context.NEW_RULE_INFO,
    infoNewRuleLong: C.strings.context.NEW_RULE_INFO_LONG,
    
    stringReset:   C.strings.context.RESET,       
    colorReset:    C.colors.context.RESET,    
    infoReset:     C.strings.context.RESET_INFO,
    infoResetLong: C.strings.context.RESET_INFO_LONG,
     
    stringVwv:   C.strings.context.VWV,       
    colorVwv:    C.colors.context.VWV,            
    infoVwv:     C.strings.context.VWV_INFO,
    infoVwvLong: C.strings.context.VWV_INFO_LONG,
    
    stringIE:   C.strings.context.IE,       
    colorIE:    C.colors.context.IE,            
    infoIE:     C.strings.context.IE_INFO,
    infoIELong: C.strings.context.IE_INFO_LONG,
    
    trueColor:     C.colors.TRUE,    trueString:    C.strings.TRUE,      
    unknownColor:  C.colors.UNKNOWN, unknownString: C.strings.UNKNOWN,     
    falseColor:    C.colors.FALSE,   falseString:   C.strings.FALSE
  });
};

Graph.svg.on("contextmenu", Context.Bg.show);

Context.Bg.newNode = function() {
  return Context.db("PUT", C.routes.NODE, [{ id: 0,
    x: (Context.Bg.eventX - Zoom.translate()[0]) / Zoom.scale()[0],
    y: (Context.Bg.eventY - Zoom.translate()[1]) / Zoom.scale()[1],
    name: C.strings.context.NEW_VAR_TEXT, value: null
  }]);
};

Context.Bg.newRule = function() {
  return Context.db("PUT", C.routes.RULE, [{ id: 0,
    x: (Context.Bg.eventX - Zoom.translate()[0]) / Zoom.scale()[0],
    y: (Context.Bg.eventY - Zoom.translate()[1]) / Zoom.scale()[1]
  }]);
};

Context.Bg.reset = function() {
  Graph.nodeData.forEach(function(d) { Data.setValue(d, null); });
  return Context.db("POST", C.routes.NODE, Graph.nodeData);
};

Context.Bg.vwv = function() {
  return Context.db2("POST", C.routes.VWV, [], function(data) {
    if(data.messages == null) {
      Graph.updateGraph(data);
    } else {
      NoMo.show(data.messages.loops[0].NonMonotonousOdd);
    }
  });
};

Context.Bg.ie = function() {
  IE.show();
  Context.hide();
};
