Data = { 
  id:    function(d) { return d.id;    },
  x:     function(d) { return d.x;     },
  y:     function(d) { return d.y;     },
  x0:    function(d) { return d.x0;    },
  y0:    function(d) { return d.y0;    },
  name:  function(d) { return d.name;  },
  value: function(d) { return d.value; },
  
  addX: function(d,x) { d.x += x; },
  addY: function(d,y) { d.y += y; },
  
  setValue: function(d,value) { d.value = value; },
  
  line: {
    func: d3.svg.line()
            .x(function(d) { return d.x; })
            .y(function(d) { return d.y; })
            .interpolate("linear") // "basis" | "linear"
  },
  
  source: function(d) { return d.source; },
  target: function(d) { return d.target; },  
  rule:   function(d) { return d.ruleId; },
  negate: function(d) { return d.negate; },
  
  toggleNegate: function(d) { d.negate = !d.negate; }
};


