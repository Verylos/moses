NoMo = { };

NoMo.hide = function() {
  $("#" + C.ids.NOMO).css({ visibility: "hidden" });
};

NoMo.show = function(connections) {
  var nomo = $("#" + C.ids.NOMO);
  nomo.css({ left: 10, top: 10, visibility: "visible" });  
  
  var header = "<center><span class='bigfont'>" + C.strings.context.NOMO_HEAD 
             + "</span></center><span class='smallfont'>" 
             + C.strings.context.NOMO_DESC + "</span>";
  
  var entries = connections.map(function(connection) 
         { return NoMo.createListEntry(connection[0], connection[1]); });
  
  var buttons = "<center><input type='button' class='button medium black' "
            + " onClick='NoMo.hide()' style='width:50%' "
            + " value='" + C.strings.context.SUBMIT + "' /></center>";
  
  nomo.html(header + "<hr>" + entries.join("") + "<hr>" + buttons);
};

NoMo.createListEntry = function(id, negated) {
  return "<input type='button' class='button medium gray' "
            + " onClick='NoMo.zoomTo(" + id + ")' style='width:100%' "
            + " value='" + Graph.getNodeById(id).name + "' />"
         + "<div style='height:5px' />";
};

NoMo.zoomTo = function(id) {
  var node = Graph.getNodeById(id);
  translateX = $(window).width() / 2 - node.x;
  translateY = $(window).height() / 2 - node.y;
  Graph.display.attr({ transform: "translate(" + translateX + "," 
                      + translateY + ")scale(" + Zoom.listener.scale() + ")" });
  Zoom.listener.translate([translateX, translateY]);  
};
