Scroll = {
  LEFT:  "l", 
  RIGHT: "r", 
  UP:    "u", 
  DOWN:  "d",
  
  SPEED: 7,
  BOUNDS: 20
};

Scroll.scroll = function(direction) {
  var speed = Scroll.SPEED;
  if(Drag.active && Scroll.timer) {
    var translateCoords = Zoom.transform(), translateX = 0, translateY = 0;
    if (direction == Scroll.LEFT || direction == Scroll.RIGHT) {
      translateX = translateCoords.translate[0] + 
          (direction == Scroll.LEFT ? speed : -speed);
      translateY = translateCoords.translate[1];  
    } else if (direction == Scroll.UP || direction == Scroll.DOWN) {
      translateX = translateCoords.translate[0];
      translateY = translateCoords.translate[1] + 
          (direction == Scroll.UP ? speed : -speed);
    }
    Graph.display
      .attr({ transform: "translate(" + translateX + "," 
        + translateY + ")scale(" + Zoom.listener.scale() + ")" });
    Zoom.listener.translate([translateX, translateY]);  
    setTimeout(function() { Scroll.scroll(direction, 10); });
  }
};

Graph.svg.append("rect").attr({ 
  id: C.ids.SCROLL_LEFT, opacity: 0, x: 0, y: 0,
  height: $(window).height(), width: Scroll.BOUNDS 
}).on("mouseover", function() { Scroll.timer = true; Scroll.scroll(Scroll.LEFT);
}).on("mouseout", function() { Scroll.timer = false; });

Graph.svg.append("rect").attr({ 
  id: C.ids.SCROLL_RIGHT, opacity: 0, x: $(window).width() - Scroll.BOUNDS, y: 0,
  height: $(window).height(), width: Scroll.BOUNDS 
}).on("mouseover", function() { Scroll.timer = true; Scroll.scroll(Scroll.RIGHT);
}).on("mouseout", function() { Scroll.timer = false; });

Graph.svg.append("rect").attr({ 
  id: C.ids.SCROLL_UP, opacity: 0, x: 0, y: 0,
  height: Scroll.BOUNDS, width: $(window).width()
}).on("mouseover", function() { Scroll.timer = true; Scroll.scroll(Scroll.UP);
}).on("mouseout", function() { Scroll.timer = false; });

Graph.svg.append("rect").attr({ 
  id: C.ids.SCROLL_DOWN, opacity: 0, x: 0, y: $(window).height() - Scroll.BOUNDS,
  height: Scroll.BOUNDS, width: $(window).width()
}).on("mouseover", function() { Scroll.timer = true; Scroll.scroll(Scroll.DOWN);
}).on("mouseout", function() { Scroll.timer = false; });

