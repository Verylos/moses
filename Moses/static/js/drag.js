Drag = {  
  active: false
};

Drag.start = function(d) {
  Drag.active = true; 
  d3.event.sourceEvent.stopPropagation();
};  
  
Drag.drag = function(d) { 
  if(Drag.active) {
    Data.addX(d, d3.event.dx);
    Data.addY(d, d3.event.dy);
    d3.select(this)
      .attr({ transform: "translate(" + Data.x(d) + "," + Data.y(d) + ")" });
      
    // Update Conditions  
    Graph.condData.filter(function(c) { 
          return Data.source(c) == Data.id(d) || Data.rule(c) == Data.id(d); })
      .forEach(function(c) {
          $("#" + C.ids.COND_ID + Data.id(c)).attr("d", Graph.drawCond(c));
    }); 
    // Update Conclusions
    Graph.concData.filter(function(c) { 
          return Data.target(c) == Data.id(d) || Data.rule(c) == Data.id(d); })
      .forEach(function(c) {
          $("#" + C.ids.CONC_ID + Data.id(c)).attr("d", Graph.drawConc(c));
    });
  }
};

Drag.end = function() {
  var nodePattern = /^node/,
      d = this.__data__;
  
  if(nodePattern.exec(Data.id(this)) != null) {
    // Node
    $.ajax(C.routes.NODE, {
      type: "POST", success: Graph.updateData,
      data: JSON.stringify([{
        id:    Data.id(d),
        x:     Data.x(d),    y:     Data.y(d),
        name:  Data.name(d), value: Data.value(d)
      }])
    });
  } else {
    // Rule
    $.ajax(C.routes.RULE, {
      type: "POST", success: Graph.updateData,
      data: JSON.stringify([{
        id: Data.id(d),
        x:  Data.x(d),  y:  Data.y(d)
      }])
    });
  }  
  Drag.active = false;
};

Drag.listener = d3.behavior.drag()
  .on("dragstart", Drag.start)
  .on("drag",      Drag.drag)
  .on("dragend",   Drag.end);
