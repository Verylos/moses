# README #

## Moses ##
Moses (Moses Open Source Expert System) is a simple expert system for learning purposes.
It is a student project intended to help students taking their first steps in artificial intelligence by providing a simple platform for knowledge representation via propositional logic in graphs.

### Installation (Tested for Ubuntu Trusty Tahr) ###
Requirements:

* 64 Bit Distribution (due to open [issue](https://github.com/yesodweb/persistent/issues/356) with persistent)
* GHC Version >= 7.6

Packages:

* cabal-install
* alex
* happy
* zlibc
* zlib1g
* zlib1g-dev
* git

## Steps ##
The following steps should lead to a running Moses server:
```
git clone https://bitbucket.com/Verylos/moses.git Moses
cd Moses/Moses
cabal update && cabal install && cabal build
./Moses
```
## Usage ##
After successful installation and server run, start a browser with enabled Javascript.
You can test moses by calling the default adress localhost:3000 which should start the frontend.